import Vue from "vue"

import siteConfigConstants from '~/store/modules/site-config/constants'
Vue.prototype.$siteConfigConstants = siteConfigConstants

import mindTestConstants from '~/store/modules/mind-test/constants'
Vue.prototype.$mindTestConstants = mindTestConstants
