import siteConfig from './modules/site-config'
import mindTest from './modules/mind-test'

export const state = () => ({})

export const mutations = {}

export const modules = {
  siteConfig,
  mindTest
}
