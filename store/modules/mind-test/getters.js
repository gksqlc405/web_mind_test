import Constants from './constants'
import state from './state'

export default {
  [Constants.GET_CURRENT_LEVEL]: (state) => {
    return state.currentLevel
  },

  [Constants.GET_MIND_TESTS]: (state) => {
    return state.mindTests
  },

  [Constants.GET_SELECTED_TESTS]: (state) => {
    return state.selectedTests
  },
  [Constants.GET_MIND_TEST_RESULTS]: (state) => {
    return state.mindTestResults
  },

}
