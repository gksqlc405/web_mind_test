import Constants from './constants'
import state from './state'

export default {
  [Constants.FETCH_CURRENT_LEVEL]: (state, payload) => {
      state.currentLevel = payload
  },
  [Constants.FETCH_SELECTED_TESTS]: (state, payload) => {
      let tempSelectedTests = state.selectedTests   //메모리 문제가 터지기 때문에 복제해서 변수 값을 넣어준다
      tempSelectedTests.push(payload)

      state.selectedTests = tempSelectedTests
  },
  [Constants.FETCH_GAME_RESET]: (state) => {
    state.currentLevel = 0
    state.selectedTests = []
  },

}
