const state = () => ({
    currentLevel: 0,
    mindTests: [
      {
          'photo': 'sdfsf.jpg',
          'question': '길을 가다가 거북이를 보았습니다.<br />어떻게 하시겠습니까',
          'answer': [
              {
                'label': '먹이를 준다',
                'point': 10
              },
            {
              'label': '때린다',
              'point': 7
            },
            {
              'label': '키운다',
              'point': 5
            },
            {
              'label': '무시한다',
              'point': 2
            },

          ]
      },
      {
        'photo': 'sdfsf',
        'question': '길을 가다가 호랑이를 보았습니다.<br />어떻게 하시겠습니까',
        'answer': [
          {
            'label': '먹이를 준다',
            'point': 10
          },
          {
            'label': '때린다',
            'point': 7
          },
          {
            'label': '키운다',
            'point': 5
          },
          {
            'label': '무시한다',
            'point': 2
          },

        ]
      },
      {
        'photo': 'sdfsf',
        'question': '길을 가다가 사자를 보았습니다.<br />어떻게 하시겠습니까',
        'answer': [
          {
            'label': '먹이를 준다',
            'point': 10
          },
          {
            'label': '때린다',
            'point': 7
          },
          {
            'label': '키운다',
            'point': 5
          },
          {
            'label': '무시한다',
            'point': 2
          },

        ]
      },
      {
        'photo': 'sdfsf',
        'question': '길을 가다가 늑대를 보았습니다.<br />어떻게 하시겠습니까',
        'answer': [
          {
            'label': '먹이를 준다',
            'point': 10
          },
          {
            'label': '때린다',
            'point': 7
          },
          {
            'label': '키운다',
            'point': 5
          },
          {
            'label': '무시한다',
            'point': 2
          },

        ]
      },
      {
        'photo': 'sdfsf',
        'question': '길을 가다가 펭귄을 보았습니다.<br />어떻게 하시겠습니까',
        'answer': [
          {
            'label': '먹이를 준다',
            'point': 10
          },
          {
            'label': '때린다',
            'point': 7
          },
          {
            'label': '키운다',
            'point': 5
          },
          {
            'label': '무시한다',
            'point': 2
          },

        ]
      },
      {
        'photo': 'sdfsf',
        'question': '길을 가다가 토끼를 보았습니다.<br />어떻게 하시겠습니까',
        'answer': [
          {
            'label': '먹이를 준다',
            'point': 10
          },
          {
            'label': '때린다',
            'point': 7
          },
          {
            'label': '키운다',
            'point': 5
          },
          {
            'label': '무시한다',
            'point': 2
          },

        ]
      },
      {
        'photo': 'sdfsf',
        'question': '길을 가다가 비둘기를 보았습니다.<br />어떻게 하시겠습니까',
        'answer': [
          {
            'label': '먹이를 준다',
            'point': 10
          },
          {
            'label': '때린다',
            'point': 7
          },
          {
            'label': '키운다',
            'point': 5
          },
          {
            'label': '무시한다',
            'point': 2
          },

        ]
      },
      {
        'photo': 'sdfsf',
        'question': '길을 가다가 강아지를 보았습니다.<br />어떻게 하시겠습니까',
        'answer': [
          {
            'label': '먹이를 준다',
            'point': 10
          },
          {
            'label': '때린다',
            'point': 7
          },
          {
            'label': '키운다',
            'point': 5
          },
          {
            'label': '무시한다',
            'point': 2
          },

        ]
      },
      {
        'photo': 'sdfsf',
        'question': '길을 가다가 고양이를 보았습니다.<br />어떻게 하시겠습니까',
        'answer': [
          {
            'label': '먹이를 준다',
            'point': 10
          },
          {
            'label': '때린다',
            'point': 7
          },
          {
            'label': '키운다',
            'point': 5
          },
          {
            'label': '무시한다',
            'point': 2
          },

        ]
      },
      {
        'photo': 'sdfsf',
        'question': '길을 가다가 수달를 보았습니다.<br />어떻게 하시겠습니까',
        'answer': [
          {
            'label': '먹이를 준다',
            'point': 10
          },
          {
            'label': '때린다',
            'point': 7
          },
          {
            'label': '키운다',
            'point': 5
          },
          {
            'label': '무시한다',
            'point': 2
          },

        ]
      }
    ],
    selectedTests: [],
    mindTestResults: [
      {
          'text': '넌 굉장히 냉정하구나'
      },
      {
        'text': '넌 굉장히 냉정하구나'
      },
      {
        'text': '넌 굉장히 냉정하구나'
      },
      {
        'text': '넌 굉장히 냉정하구나'
      },
      {
        'text': '넌 굉장히 냉정하구나'
      },
      {
        'text': '넌 굉장히 냉정하구나'
      },
      {
        'text': '넌 굉장히 냉정하구나'
      },
      {
        'text': '넌 굉장히 냉정하구나'
      },
      {
        'text': '넌 굉장히 냉정하구나'
      },
      {
        'text': '넌 굉장히 냉정하구나'
      },
    ]
})

export default state
