export default {
    GET_CURRENT_LEVEL: 'mind-test/getCurrentLevel',
    GET_MIND_TESTS: 'mind-test/getMindTests',
    GET_SELECTED_TESTS: 'mind-test/getSelectedTests',
    GET_MIND_TEST_RESULTS: 'mind-test/getMindTestResults',

  //수정에 개념으로 set 과 put 은 fetch로 쓴다
    FETCH_CURRENT_LEVEL: 'mind-test/fetchCurrentLevel',
    FETCH_SELECTED_TESTS: 'mind-test/fetchSelectedTests',
    FETCH_GAME_RESET: 'mind-test/fetchGameReset'
}
